# OS X front-end development environment setup #

Description to set up a front-end development environment on OS X

### Show/hide hidden files ###

Hide:
```
$ defaults write com.apple.finder AppleShowAllFiles -boolean false
$ killall Finder
```

Show:
```
$ defaults write com.apple.finder AppleShowAllFiles -boolean true
$ killall Finder
```

### Asepsis ###
[Asepsis ](http://asepsis.binaryage.com/) prevents creation of .DS_Store files. It redirects their creation into a special folder.


Troubleshooting:
```
$ asepsisctl diagnose
$ Updates
```

Uninstallation:
```
$ asepsisctl uninstall
$ sudo reboot
```

### iTerm 2 ###

[iTerm 2](https://www.iterm2.com/) is a replacement for Terminal and the successor to iTerm

### Change shell ###

Show current shell:
```
$ echo $SHELL
```

Change shell:

1. System Preferences > Users & Groups
1. Unlock if necessary
1. Right click on user and choose Advanced Options...
1. Login shell > /bin/zsh

### Change Terminal font ###

Article: [Top 10 programming fonts](http://hivelogic.com/articles/top-10-programming-fonts/)

1. [Download Ubunto](http://font.ubuntu.com/)
1. Install the Ubunto mono font
1. iTerm2 > Preferences > Profiles > Text > Change Font > Ubunto Mono
1. Select 14pt for the font size

### Change Terminal theme ###

* [Some IDEA Color Themes](http://www.ideacolorthemes.org/themes/)
* [Dracula theme](https://github.com/zenorocha/dracula-theme)

Download the Darcula theme
```
$ git clone https://github.com/zenorocha/dracula-theme.git
```
1. iTerm2 > Preferences > Profiles > Colors Tab
1. Click Load Presets...
1. Click Import...
1. Select the iterm/Dracula.itermcolors file
1. Select the Dracula from Load Presets...

### Install Xcode tools (Apple command line developer tools) ###

```
$ xcode-select --install
```

### Install Homebrew package manager ###

[Homebrew website](http://brew.sh/)

```
$ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
$ brew doctor
```

### Install Linux/Unix tree command ###

```
$ brew install tree
```

### Install Oh My Zsh ###

[Oh My Zsh](https://github.com/robbyrussell/oh-my-zsh) is an open source, community-driven framework for managing your zsh configuration.

Install:
```
$ curl -L https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh
```

Plugins:
```
plugins=(bower brew gem git github gitignore grunt node npm osx ruby rvm sublime vagrant vim-interaction xcode)
```

* Default Theme: ```ZSH_THEME="robbyrussell"```
* Random Theme: ```ZSH_THEME="random"```
* Custom Theme: ```ZSH_THEME="mycustomtheme"```

If you want to override any of the default behaviours, just add a new file (ending in .zsh) in the custom/ directory.

Custom prompt/theme shell script:
```
local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ %s)"

PROMPT='$fg[blue]%n$reset_color@$fg[cyan]%m$fg[yellow] %~
${ret_status}%{$fg_bold[green]%}%p %{$fg[cyan]%}%c %{$fg_bold[blue]%}$(git_prompt_info)%{$fg_bold[blue]%} % %{$reset_color%}'

ZSH_THEME_GIT_PROMPT_PREFIX="git:(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%}) %{$fg[yellow]%}✗%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"

RPROMPT="%T"
```

### Shell Aliases and Functions ###

[.zshrc example](https://gist.github.com/zanshin/1142739)
[Installing and configuring the Oh My ZSH shell](http://www.stevendobbelaere.be/installing-and-configuring-the-oh-my-zsh-shell/)

Some snippets of aliases and functions:
```
autoload -U colors && colors

#PROMPT="
#$fg[blue]%n$reset_color@$fg[cyan]%m$fg[yellow] %~
#$fg[magenta]→ $reset_color"

PROMPT="
$fg[blue]%n$fg[magenta]@$fg[cyan]%m$fg[yellow] %~
$fg[green]➜ $reset_color"

RPROMPT="%T"

# -------------------------------------------------------------------
# Bower aliases
# -------------------------------------------------------------------

alias bower='noglob bower'

# -------------------------------------------------------------------
# Git aliases
# -------------------------------------------------------------------

alias ga='git add'
alias gco='git checkout'
alias gst='git status'
alias gc='git commit'

# -------------------------------------------------------------------
# Other aliases
# -------------------------------------------------------------------

alias lsa='ls -la'
alias cl='clear'

# -------------------------------------------------------------------
# FUNCTIONS
# -------------------------------------------------------------------

# Pre command
#function precmd () {
#	ls
#}

# Create directory and cd to it
function take () {
	mkdir $1
	cd $1
}

# Setup simple python server
funtion server () {
	if [ $1]
	then
		local port="$1"
	else
		local port="8000"
	fi
	open "http://localhost:$port" && python -m SimpleHTTPServer "$port"
}
```

### Install git ###

[Git website](http://git-scm.com/)

```
$ git --version
```

### Configurate git ### 

```
$ git config —-global user.name ‘Your Name’
$ git config —-global user.email ‘your@emial.com’
$ git config —-global color.ui true
```

### Create SSH key ###

[Github generating SSH keys tutorial](website: https://help.github.com/articles/generating-ssh-keys/)

```
$ ssh-keygen -t rsa -C ‘your@emial.com’
$ eval ”$(ssh-agent -s)”
$ ssh-add ~/.ssh/id_rsa
```

### Give SSH public key to github/bitbucket account ###

```
$ cat ~/.ssh/id_rsa.pub | pbcopy
```

Go to bitbucket or github, under profile select ssh-key >> add Key
Give your key a label or title (computer name yyyy-mm-dd) and paste the SSH public key.

Test connection: ```$ ssh -T git@github.com```

### Install NodeJS/npm ###

* [NodeJS website](https://nodejs.org/)
* [Install using installer package](http://nodejs.org/download/)
* [Install using homebrew](http://thechangelog.com/install-node-js-with-homebrew-on-os-x/)

Check versions:
```
$ node -—version
$ npm -—version
```

### Stop using ```sudo``` ###
```
$ sudo chown -R $USER /usr/local
```

### Install Node version manager ###

```
$ sudo npm install -g n
$ n ls
$ n —-help
$ n —-latest
$ n —-stable
$ sudo n latest
$ sudo n stable
$ sudo n
```

### Install Sass ###

[Sass website](http://sass-lang.com/)

Install:
```
$ sudo gem install sass
```

Check version:
```
$ sass --version
```

### Install Bower ###

[Bower website](http://bower.io/)

Install Bower:
```
$ npm install -g bower
```

### Bower configuration ###
In ```.bowerrc``` file:
```
{ "directory": "app/bower_components" }
```

More configuration options can be found [here](http://bower.io/docs/config/)

### Install Grunt command line interface ###

[Grunt website](http://gruntjs.com/)

Install grunt-cli:
```
$ npm install -g grunt-cli
```

### Install Yeoman ###

[Yeoman website](http://yeoman.io/)

Install Yeoman:
```
$ npm install -g yo
```

### Install Yeoman generators ###

[Yeoman generators](http://yeoman.io/generators/)

It's preferred to use the `$ yo` command line to in/uninstall or update generators.  

Otherwise you can use the `$ npm` command:

Install a generator:
```
$ npm install -g generator-angular
```

Uninstall a generator:
```
$ npm uninstall -g generator-angular
```

Update generator:
```
$ npm update -g generator-angular
```

Creating a generator: [http://yeoman.io/authoring/](http://yeoman.io/authoring/)  

Popular generators:

* [angular](https://github.com/yeoman/generator-angular)
* [gulp-angular](https://github.com/Swiip/generator-gulp-angular)
* [webapp (Grunt)](https://github.com/robwierzbowski/generator-jekyllrb)
* [gulp-webapp](https://github.com/yeoman/generator-gulp-webapp)
* [ionic](https://github.com/diegonetto/generator-ionic)

### IDEA/Code editors ###

* [Sublime Text](http://www.sublimetext.com/)
* [WebStorm](http://www.jetbrains.com/webstorm/)
* [Brackets](http://brackets.io/)
* [Atom](https://atom.io/)
* [MacVim](https://code.google.com/p/macvim/)